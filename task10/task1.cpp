#include <iostream.h>
#include <vector>
#include <algorithm>
#include <fstream>
#include <string>
using namespace std;

int value(string &_str) {
	int sum = 0;
	for (string::iterator it = _str.begin(); it < _str.end(); it++) {
		sum += (int)(*it - 'A' + 1);
	}
	return sum;
}

int main() {
	vector<string>spisok;
	string s;
	fstream a("names.txt");
	while (getline(a, s, ',')) {
		s.erase(0, 1);
		s.erase(s.size() - 1, 1);
		spisok.push_back(s);
	}
	sort(spisok.begin(), spisok.end());
	int b=0;
	int c=1;
	for (vector<string>::iterator it = spisok.begin(); it < spisok.end(); it++) {
		b+= c*value(*it);
		c++;
	}
	cout << "Alfavitnoe znachenie: " << b << endl << "Kolichetvo imen: " << c-1 << endl;
	system("pause");
	return 0;
}
